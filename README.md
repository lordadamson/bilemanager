# Bile Manager

A file manager. The UI is programmed from scratch using HTML5 canvas. The backend is a go server.

<img src="HTML_5_canvas_file_manager_context_menu_and_drag_and_drop.gif">