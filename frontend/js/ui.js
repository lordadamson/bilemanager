export { init, ls, display_error };

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext("2d");

const grid_size = 64;
const padding = 15;
const top_bar_h = 30;

const folder_icon = document.getElementById('folder_icon');
const file_icon   = document.getElementById('file_icon');

canvas.addEventListener('mousemove', _on_mouse_move);
canvas.addEventListener('mousedown', _on_mouse_down);
canvas.addEventListener('mouseup',   _on_mouse_up);
canvas.addEventListener('wheel',     _on_mouse_wheel);
canvas.addEventListener("dblclick",  _on_mouse_double_click);


document.defaultView.addEventListener('resize', _on_win_resize);

var windows = [];

var clipboard = {
    sources: [],
    destination: null,
    operation: null,
    ready: false
};

function clear(win)
{
    ctx.fillStyle = '#222';
    ctx.fillRect(win.x, win.y, win.w, win.h);
}

function calculate_windows_sizes(windows)
{
    for(var i = 0; i < windows.length; i++)
    {
        windows[i].w = canvas.width / windows.length;
        windows[i].x = i * windows[i].w;
    }
}

function init(user_data, current_directory, on_cd_request, request_operation, request_split)
{
    ctx.canvas.width  = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    var win = {
        x: 0,
        y: top_bar_h,
        w: canvas.width,
        h: canvas.height,
        listing: null,
        y_scroll: top_bar_h,
        selected_files: [],
        dragged_files: [],
        left_click: false,
        selection_box: null,
        user_data: user_data,
        on_cd_request: on_cd_request,
        current_directory: current_directory,
        request_operation: request_operation,
        request_split: request_split,
    };

    windows.push(win);

    calculate_windows_sizes(windows);

    for(var i = 0; i < windows.length; i++)
    {
        ls(windows[i], windows[i].current_directory, windows[i].listing);
    }

    return win;
}

function win_x_bounds_begin(win)
{
    return win.x + padding;
}

function win_y_bounds_begin(win)
{
    return win.y + padding + top_bar_h;
}

function win_x_bounds_end(win)
{
    return (win.x + win.w) - padding - grid_size;
}

function draw_folder(x, y)
{
    ctx.drawImage(folder_icon, x, y, grid_size, grid_size * folder_icon.height / folder_icon.width);
}

function draw_file(x, y)
{
    ctx.drawImage(file_icon, x, y, grid_size, grid_size * file_icon.height / file_icon.width);
}

function file_in_drawing_range(win, file, y)
{
    if(file.y < y - grid_size)
    {
        return false;
    }

    if(file.y > y + win.h)
    {
        return false;
    }

    return true;
}

function files_are_equal(file1, file2)
{
    return file1.x == file2.x && file1.y == file2.y;
}

function file_is_selected(win, file)
{
    const selected = win.selected_files;

    for(var i = 0; i < selected.length; i++)
    {
        if(files_are_equal(file, selected[i]))
        {
            return true;
        }
    }

    return false;
}

function draw_text(text, x, y, font_size, color, align)
{
    if(align == null)
    {
        ctx.textAlign = "left";
    }

    ctx.font = font_size + "px Arial";
    ctx.fillStyle = color;
    ctx.textAlign = align;
    ctx.fillText(text, x, y);
}

function draw_rectangle(x, y, w, h, color, alpha)
{
    ctx.beginPath();
    ctx.globalAlpha = alpha;
    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);
    ctx.stroke();
    ctx.globalAlpha = 1;
}

function draw_top_bar(win)
{
    draw_rectangle(win.x, 0, win.w, top_bar_h, "#000", 1);

    // TODO: the upper half should be the address bar

    // the lower half should be:
    // up, favorites, connections, split
    var x = win.x;
    draw_rectangle(x, 0, top_bar_h, top_bar_h, "#339", 1);
    x += top_bar_h;
    draw_rectangle(x, 0, top_bar_h, top_bar_h, "#933", 1);
    x += top_bar_h;
    draw_rectangle(x, 0, top_bar_h, top_bar_h, "#393", 1);
    x += top_bar_h;
    draw_text(win.current_directory, x + padding, 3*top_bar_h/4, top_bar_h/2, "#eee", "left");

    if(windows.length < 2)
    {
        x = win.x + (win.w - top_bar_h);
        draw_rectangle(x, 0, top_bar_h, top_bar_h, "#399", 1);
    }
}

function top_bar_get_clicked_button(win, x)
{
    if(x < win.x + top_bar_h)
    {
        return 'up';
    }
    else if(x < win.x + 2 * top_bar_h)
    {
        return 'favorites';
    }
    else if(x < win.x + 3 * top_bar_h)
    {
        return 'connections';
    }
    else if(windows.length < 2 && x > win.x + (win.w - top_bar_h))
    {
        return 'split';
    }

    return 'address_bar';
}

function handle_top_bar_click(win, x)
{
    const clicked_btn = top_bar_get_clicked_button(win, x);

    if(clicked_btn == 'split')
    {
        win.request_split(win.user_data);
    }
    else if(clicked_btn == 'up')
    {
        const tokens_ = win.current_directory.split('/');
        var tokens = [];

        for(var i = 0; i < tokens_.length; i++)
        {
            if(tokens_[i] != "")
            {
                tokens.push(tokens_[i]);
            }
        }

        if(tokens.length < 1)
        {
            return;
        }

        var requested_directory = "";

        for(var i = 0; i < tokens.length - 1; i++)
        {
            requested_directory += "/" + tokens[i];
        }

        requested_directory += "/";

        win.on_cd_request(win.user_data, requested_directory);
    }
}

function draw(win)
{
    clear(win);

    if(win.listing == null)
    {
        return;
    }

    for(var i = 0; i < win.listing.length; i++)
    {
        const file = win.listing[i];

        if(!file_in_drawing_range(win, file, win.y_scroll))
        {
            continue;
        }

        if(file_is_selected(win, file))
        {
            // draw selection box
            draw_rectangle(file.x - padding/2, file.y - win.y_scroll - padding/2, grid_size + padding, grid_size + padding, "#a9cdfc", 0.2);
        }

        if(file.type == 'dir')
        {
            if(file.dragged_x != null)
            {
                draw_folder(file.dragged_x, file.dragged_y);
            }
            else
            {
                draw_folder(file.x, file.y - win.y_scroll);
            }
        }
        else
        {
            if(file.dragged_x != null)
            {
                draw_file(file.dragged_x, file.dragged_y);
            }
            else
            {
                draw_file(file.x, file.y - win.y_scroll);
            }
        }

        ctx.font = "12px Arial";
        ctx.fillStyle = "#eee";
        ctx.textAlign = "center";

        if(file.dragged_x != null)
        {
            ctx.fillText(file.display_name, (file.dragged_x + file.dragged_x + grid_size) / 2, file.dragged_y + grid_size + 3);
        }
        else
        {
            ctx.fillText(file.display_name, (file.x + file.x + grid_size) / 2, file.y - win.y_scroll + grid_size + 3);
        }
    }

    if(win.selection_box != null)
    {
        const box = win.selection_box;
        draw_rectangle(box.x, box.y, box.w, box.h, "#a9cdfc", 0.2);
    }
    if(win.context_menu != null)
    {
        draw_context_menu(win.context_menu);
    }

    draw_top_bar(win);
}

function ls(win, current_directory, listing)
{
    if(listing == null)
    {
        return;
    }

    win.listing = listing;
    win.current_directory = current_directory;

    var x = win_x_bounds_begin(win);
    var y = win_y_bounds_begin(win);

    for(var i = 0; i < win.listing.length; i++)
    {
        const file = win.listing[i];
        file.display_name = file.name;
        if(file.name.length * 6 > grid_size)
        {
            file.display_name = file.name.substr(0, grid_size/6);
            file.display_name += '...';
        }

        file.x = x;
        file.y = y;

        x += grid_size + padding;

        if(x >= win_x_bounds_end(win))
        {
            x = win_x_bounds_begin(win);
            y += grid_size + padding;
        }
    }

    // var connection_input = document.getElementById('address_input1');
    // connection_input.style.left = 0;
    // connection_input.style.height = top_bar_h;
    // connection_input.innerHTML = "local";

    draw(win);   
}

function display_error(win, error)
{
    ctx.font = "30px Arial";
    ctx.fillStyle = "red";
    ctx.textAlign = "center";
    ctx.fillText(error, win.w/2, win.h/2);
}

function test_box_collision(box, x, y)
{
    if(x > box.x && x < box.x + box.w && y > box.y && y < box.y + box.h)
    {
        return true;
    }

    return false;
}

function test_two_boxes_collision(box1_, box2_)
{
    // cloning the objects because we're gonna change their values
    let box1 = Object.assign({}, box1_);
    let box2 = Object.assign({}, box2_);

    if(box1.w < 0)
    {
        box1.x += box1.w;
        box1.w *= -1;
    }
    if(box2.w < 0)
    {
        box2.x += box2.w;
        box2.w *= -1;
    }
    if(box1.h < 0)
    {
        box1.y += box1.h;
        box1.h *= -1;
    }
    if(box2.h < 0)
    {
        box2.y += box2.h;
        box2.h *= -1;
    }

    if(box1.x + box1.w < box2.x || box2.x + box2.w < box1.x)
    {
        return false;
    }

    if(box1.y + box1.h < box2.y || box2.y + box2.h < box1.y)
    {
        return false;
    }

    return true;
}

function get_clicked_file(y_scroll, x, y, listing)
{
    for(var i = 0; i < listing.length; i++)
    {
        if(test_box_collision({x: listing[i].x, y: listing[i].y - y_scroll, w: grid_size, h: grid_size}, x, y))
        {
            return listing[i];
        }
    }

    return null;
}

function get_window_from_mouse_event(e)
{
    var win = windows[0];

    if(windows.length > 1 && e.clientX > windows[0].w)
    {
        win = windows[1];
    }

    return win;
}

function make_selection_box(win, x, y)
{
    if(win.selection_box == null)
    {
        win.selection_box = {x: x, y: y, w: 0, h: 0};
        return;
    }

    const box = win.selection_box;

    box.w = x - box.x;
    box.h = y - box.y;
}

function test_for_selected_files(win)
{
    win.selected_files = [];

    if(win.selection_box == null)
    {
        return;
    }

    const files = win.listing;

    for(var i = 0; i < files.length; i++)
    {
        if(!file_in_drawing_range(win, files[i], win.y_scroll))
        {
            continue;
        }

        const file_box = {
            x: files[i].x - padding/2,
            y: files[i].y - win.y_scroll - padding/2,
            w: grid_size + padding,
            h: grid_size + padding
        };

        if(test_two_boxes_collision(file_box, win.selection_box))
        {
            win.selected_files.push(files[i]);
        }
    }
}

function _on_mouse_move(e)
{
    const win = get_window_from_mouse_event(e);

    const x = e.clientX;
    const y = e.clientY;

    if(win.dragged_files.length == 0 && win.left_click)
    {
        make_selection_box(win, x, y);
        test_for_selected_files(win);
        draw(win);
        draw_top_bar(win)
        return;
    }

    if(win.left_click != true)
    {
        return;
    }
    
    for(var i = 0; i < win.dragged_files.length; i++)
    {
        const file = win.dragged_files[i];
        
        if(file.dragged_x == null)
        {
            file.dragged_x = file.x;
        }
        if(file.dragged_y == null)
        {
            file.dragged_y = file.y - win.y_scroll;
        }

        file.dragged_x += e.movementX;
        file.dragged_y += e.movementY;
    }

    draw(win);
    
}

function on_left_click(win, x, y, ctrl)
{
    if(y < top_bar_h)
    {
        handle_top_bar_click(win, x);
        return;
    }

    win.left_click = true;

    const file = get_clicked_file(win.y_scroll, x, y, win.listing);

    if(file !== null)
    {
        if(ctrl)
        {
            if(!file_is_selected(win, file))
            {
                win.selected_files.push(file);
            }

            win.dragged_files = win.selected_files;
        }
        else
        {
            win.selected_files = [file];
            win.dragged_files = [file];
        }
    }
    else
    {
        win.selected_files = [];
    }
}

function draw_context_menu(context_menu)
{
    var y = context_menu.y;
    const x = context_menu.x;
    const w = context_menu.w;
    const cell_height = context_menu.cell_height;
    const entries = context_menu.entries;

    ctx.beginPath();
    ctx.fillStyle = "#eee";
    ctx.fillRect(x, y, context_menu.w, context_menu.h);
    ctx.stroke();

    ctx.font = "15px Arial";
    ctx.fillStyle = "#333";
    ctx.textAlign = "center";

    y += cell_height;

    for(var i = 0; i < entries.length; i++)
    {
        ctx.fillText(entries[i], (x + x + w) / 2, y); y += cell_height;
    }
}

function make_context_menu(x, y, entries)
{
    const context_menu_cell_height = 20;
    const context_menu_width = 100;
    const context_menu_height = 10 + context_menu_cell_height * entries.length;

    return {
        x: x, y: y,
        w: context_menu_width, h: context_menu_height,
        cell_height: context_menu_cell_height, entries: entries
    };
}

function make_context_menu_file(x, y)
{
    return make_context_menu(x, y, ["Cut", "Copy", "Delete", "Rename"]);
}

function make_context_menu_multiple_files(x, y)
{
    return make_context_menu(x, y, ["Cut", "Copy", "Delete"]);
}

function make_context_menu_dir(x, y)
{
    return make_context_menu(x, y, ["Cut", "Copy", "Paste", "Delete", "Rename"]);
}

function make_context_menu_empty(x, y)
{
    return make_context_menu(x, y, ["Paste"]);
}

function on_right_click(win, x, y)
{
    const file = get_clicked_file(win.y_scroll, x, y, win.listing);
    
    if(file === null)
    {
        win.context_menu = make_context_menu_empty(x, y);
        win.selected_files = [];
        return;
    }

    if(win.selected_files.length > 0)
    {
        for(var i = 0; i < win.selected_files.length; i++)
        {
            if(files_are_equal(file, win.selected_files[i]))
            {
                win.context_menu = make_context_menu_multiple_files(x, y);
                return;
            }
        }
    }
    
    if(file.type == 'dir')
    {
        win.context_menu = make_context_menu_dir(x, y);
        win.selected_files = [file];
    }
    else
    {
        win.context_menu = make_context_menu_file(x, y);
        win.selected_files = [file];
    }
}

function context_menu_get_entry_clicked(context_menu, x, y)
{
    var cy = context_menu.y;
    const cx = context_menu.x;

    for(var i = 0; i < context_menu.entries.length; i++)
    {
        const box = {
            x: cx,
            y: cy,
            w: context_menu.w,
            h: context_menu.cell_height
        };

        if(test_box_collision(box, x, y))
        {
            return context_menu.entries[i];
        }

        cy += context_menu.cell_height;
    }
}

function insert_selected_files(array, current_directory, selected_files)
{
    for(var i = 0; i < selected_files.length; i++)
    {
        array.push(current_directory + selected_files[i].name);
    }
}

function handle_context_menu_click(win, x, y)
{
    const clicked_entry = context_menu_get_entry_clicked(win.context_menu, x, y);

    console.log(clicked_entry);

    clipboard.ready = false;

    if(clipboard.sources == null)
    {
        array = [];
    }

    if(clicked_entry == "Cut")
    {
        insert_selected_files(clipboard.sources, win.current_directory, win.selected_files);
        clipboard.operation = clicked_entry;
    }
    if(clicked_entry == "Copy")
    {
        insert_selected_files(clipboard.sources, win.current_directory, win.selected_files);
        clipboard.operation = clicked_entry;
    }
    if(clicked_entry == "Paste")
    {
        if(clipboard.operation == null)
        {
            return;
        }

        if(win.selected_files < 1)
        {
            clipboard.destination = win.current_directory;
        }
        else
        {
            clipboard.destination = win.selected_files[0];
        }
        
        clipboard.ready = true;
    }
    if(clicked_entry == "Rename")
    {
        insert_selected_files(clipboard.sources, win.current_directory, win.selected_files);
        clipboard.operation = clicked_entry;
    }
    if(clicked_entry == "Delete")
    {
        insert_selected_files(clipboard.sources, win.current_directory, win.selected_files);
        clipboard.operation = clicked_entry;
        clipboard.ready = true;
    }
}

function _on_mouse_down(e)
{
    const win = get_window_from_mouse_event(e);

    if(win.context_menu != null && test_box_collision(win.context_menu, e.clientX, e.clientY))
    {
        handle_context_menu_click(win, e.clientX, e.clientY)
        win.context_menu = null;
        if(clipboard.ready)
        {
            win.request_operation(win.user_data, clipboard);
        }
    }
    else
    {
        win.context_menu = null;
    }

    if(e.button == 1) // middle click
    {
        return;
    }

    var ctrl = false;

    if(e.ctrlKey || e.metaKey)
    {
        ctrl = true;
    }
    
    if(e.button == 2) // right click
    {
        on_right_click(win, e.clientX, e.clientY);
    }
    else if(e.button == 0) // left click
    {
        on_left_click(win, e.clientX, e.clientY, ctrl);
    }

    draw(win);
    
}

function _on_mouse_up(e)
{
    const win = get_window_from_mouse_event(e);

    win.selection_box = null;

    if(e.button == 0)
    {
        win.left_click = false;
    }

    for(var i = 0; i < win.dragged_files.length; i++)
    {
        const file = win.dragged_files[i];
        file.dragged_x = null;
        file.dragged_y = null;
    }

    if(win.dragged_files.length > 0 && win.context_menu == null)
    {
        win.dragged_files = [];
    }

    draw(win);
    
}

function _on_mouse_wheel(e)
{
    e.preventDefault();

    const win = get_window_from_mouse_event(e);

    if(win.listing.length == 0)
    {
        return;
    }

    console.log(e.key);

    const first_file = win.listing[0];
    const last_file = win.listing[win.listing.length - 1];

    if(last_file.y < win.h)
    {
        return;
    }

    if(win.y_scroll <= top_bar_h && e.deltaY < 0)
    {
        win.y_scroll = top_bar_h;
        draw(win);
        
        return;
    }

    if(win.y_scroll >= last_file.y - win.h + grid_size + padding && e.deltaY > 0)
    {
        win.y_scroll = last_file.y - win.h + grid_size + padding;
        draw(win);
        
        return;
    }

    win.y_scroll += e.deltaY;

    draw(win);
}

function _on_win_resize()
{
    canvas.width  = window.innerWidth;
    canvas.height = window.innerHeight;

    calculate_windows_sizes(windows);

    for(var i = 0; i < windows.length; i++)
    {
        ls(windows[i], windows[i].current_directory, windows[i].listing);
    }
}

function _on_mouse_double_click(e)
{
    const win = get_window_from_mouse_event(e);

    const x = e.clientX;
    const y = e.clientY;
    const file = get_clicked_file(win.y_scroll, x, y, win.listing);

    if(file == null)
    {
        return;
    }
    
    if(file.type == 'file')
    {
        return;
    }

    win.on_cd_request(win.user_data, win.current_directory + file.name + '/');
}